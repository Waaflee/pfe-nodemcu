#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <queue>
#include <string>

#include <MessageParser.hpp>

#include "config.hpp"
#include "utils/logger.hpp"

WiFiClient espClient;
PubSubClient client(espClient);
MessageParser parser;

void callback(char *topic, byte *payload, unsigned int length);

void setup() {
  // Set software serial baud to 115200;
  Serial.begin(config::uart::baudrate);

  // connecting to a WiFi network
  WiFi.begin(config::wifi::ssid, config::wifi::password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");

  // connecting to a mqtt broker
  client.setServer(config::mqtt::broker, config::mqtt::port);
  client.setCallback(callback);
  while (!client.connected()) {
    String client_id = "esp8266-client-";
    client_id += String(WiFi.macAddress());
    Serial.printf("The client %s connects to the public mqtt broker\n",
                  client_id.c_str());
    if (client.connect(client_id.c_str(), config::mqtt::username,
                       config::mqtt::password)) {
      Serial.println("Public emqx mqtt broker connected");
    } else {
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
  client.subscribe(config::mqtt::subscribe_topic);
}

void callback(char *topic, byte *payload, unsigned int length) {
  std::string message = "";
  for (auto i = 0u; i < length; i++) {
    // Serial.print((char)payload[i]);
    message += (char)payload[i];
  }
  message = parser.decode(message);
  // Serial.print('\n');
  Serial.println(message.c_str());
}

std::string uartBuffer;
std::queue<std::string> messages;

void readMessages() {
  auto data = ' ';
  while (Serial.available()) {
    Serial.readBytes(&data, 1);
    if (data == '\n') {
      messages.push(uartBuffer);
      uartBuffer = "";
    } else {
      uartBuffer += data;
    }
  }
};

void loop() {
  client.loop();

  readMessages();
  if (messages.size()) {
    auto payload = parser.encode(messages.front());
    client.publish(config::mqtt::publish_topic, payload.c_str());
    messages.pop();
  }
}
