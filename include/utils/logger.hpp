#pragma once

#ifdef LOG_LEVEL_DEBUG
#define LOG(X) Serial.println(X);
#else
#define LOG(X)
#endif