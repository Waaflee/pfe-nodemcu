#pragma once
#include <string>

namespace config {
// UART
namespace uart {
constexpr auto baudrate = 921600u;
} // namespace uart

// WiFi
namespace wifi {
constexpr auto ssid = WIFI_SSID;     // Enter your WiFi name
constexpr auto password = WIFI_PASS; // Enter WiFi password
} // namespace wifi

// MQTT Broker
namespace mqtt {
constexpr auto broker = "broker.emqx.io";
constexpr auto subscribe_topic = "pfe/map2";
constexpr auto publish_topic = "pfe/robot2";
constexpr auto username = "emqx";
constexpr auto password = "public";
constexpr auto port = 1883;
} // namespace mqtt
} // namespace config
