#pragma once
#include <ArduinoJson.hpp>
#include <string>

using ArduinoJson::deserializeJson;
using ArduinoJson::serializeJson;
using ArduinoJson::StaticJsonDocument;

class MessageParser {
private:
public:
  MessageParser(/* args */){};
  ~MessageParser(){};
  std::string decode(std::string input) {
    StaticJsonDocument<256> inputDocument;
    StaticJsonDocument<256> ouputDocument;
    deserializeJson(inputDocument, input);
    ouputDocument["x"] = inputDocument["translationTarget"];
    ouputDocument["o"] = inputDocument["rotationTarget"];
    std::string payload = "";
    serializeJson(ouputDocument, payload);
    return payload;
  }

  std::string encode(std::string input) {
    StaticJsonDocument<256> inputDocument;
    StaticJsonDocument<256> ouputDocument;
    deserializeJson(inputDocument, input);
    ouputDocument["translationDelta"] = inputDocument["x"];
    ouputDocument["rotationDelta"] = inputDocument["o"];
    std::string payload = "";
    serializeJson(ouputDocument, payload);
    return payload;
  }
};
